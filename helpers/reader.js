const readline = require('readline');

class Reader {

    _open() {
        return readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });
    }

    _close(reader) {
        reader.close();
    }

    read(label) {
        return new Promise((resolve) => {
            const reader = this._open();
            reader.question(label + ' : ', (answer) => {
                console.log(`User typed : ${answer}`);
                resolve(answer);
                this._close(reader);
            });
        })

    }
}

exports.Reader = Reader;