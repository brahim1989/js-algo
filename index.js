/**
 * main
 * @author moi
 * simple approche algorithmique:
 * -definition de variables
 * -affectations de valeurs aux variables
 *- les différents types
 * @version  1.0.0
 *
 * */

let myName = "";
let yourName = "";

// affectation de valeur Brahim a  la variable myName
myName = "brahim";
yourName = "bond";

// affichage du contenu de la variable  myName

console.log(myName); // Expected output: brahim

// changer la valeur de myName par autre chose
myName = "autre chose";
console.log(myName);

let isMale = true;
let hasADog = true;
console.log(isMale);

// changer le contenu d'une variable à une autre variable
// myName = yourName;
// yourName = "superman";
// console.log(`${myName} .... ${yourName}`);

// for (let i = 0; i < users.length; i++) {
//   console.log(users[i]);
// }
// let i = 0;
// do {
//   console.log(users[i]);
//   i++;
// } while (i < users.length);

// while (i < users.length) {
//   console.log(users[i]);
//   i++;
// }
let users = ["Brahim", "Abdel", "Rémy", "Albert", "Nesibe", "Julien"];
for (let i = 0; i < users.length; i++) {
  console.log(users[i]);
}

if (users[0] == "Brahim") {
  console.log(`salut${users[0]}`);
}
if (users[2] == "Rémy") {
  console.log(`hola ${users[2]}`);
}

let isTrue = null;
if (!isTrue) {
  console.log("je suis vrai");
} else {
  console.log("je suis faux");
}

// let isBrahim =
//   users[0] === "Brahim"
//     ? console.log("okay brahim")
//     : console.log("pas brahim");

let anyValue = 3;
switch (anyValue) {
  case 0:
  case 1:
  case 2:
    console.log("je fais partie de 0..2");
    break;
  case 3:
    console.log("je suis seulement 3");
    break;
  default:
    console.log("je ne suis rien !");
}
